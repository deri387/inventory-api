<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("subjects")->insert([
            [
                "id"  => 1,
                "name" => "PPLG",
            ],
            [
                "id"  => 2,
                "name" => "BP",
            ],
            [
                "id"  => 3,
                "name" => "TJKT",
            ],
            [
                "id"  => 4,
                "name" => "ELEKTRO",
            ],
            [
                "id"  => 5,
                "name" => "MESIN",
            ],
            [
                "id"  => 6,
                "name" => "TKR",
            ],
            [
                "id"  => 7,
                "name" => "TEKSTIL",
            ],
        ]);

        DB::table("users")->insert([
            [
                "name" => "Admin PPLG",
                "email" => "pplg@gmail.com",
                "email_verified_at" => date("Y-m-d H:i:s"),
                "password" => bcrypt("pplg123"),
                "status" => 1,
                "subject_id" => 1
            ],
            [
                "name" => "Admin BP",
                "email" => "bp@gmail.com",
                "email_verified_at" => date("Y-m-d H:i:s"),
                "password" => bcrypt("bp123"),
                "status" => 1,
                "subject_id" => 2
            ],
            [
                "name" => "Admin TJKT",
                "email" => "tjkt@gmail.com",
                "email_verified_at" => date("Y-m-d H:i:s"),
                "password" => bcrypt("tjkt123"),
                "status" => 1,
                "subject_id" => 3
            ],
            [
                "name" => "Admin ELEKTRO",
                "email" => "elektro@gmail.com",
                "email_verified_at" => date("Y-m-d H:i:s"),
                "password" => bcrypt("elektro123"),
                "status" => 1,
                "subject_id" => 4
            ],
            [
                "name" => "Admin MESIN",
                "email" => "mesin@gmail.com",
                "email_verified_at" => date("Y-m-d H:i:s"),
                "password" => bcrypt("mesin123"),
                "status" => 1,
                "subject_id" => 5
            ],
            [
                "name" => "Admin TKR",
                "email" => "tkr@gmail.com",
                "email_verified_at" => date("Y-m-d H:i:s"),
                "password" => bcrypt("tkr123"),
                "status" => 1,
                "subject_id" => 6
            ],
            [
                "name" => "Admin TEKSTIL",
                "email" => "tekstil@gmail.com",
                "email_verified_at" => date("Y-m-d H:i:s"),
                "password" => bcrypt("tekstil123"),
                "status" => 1, 
                "subject_id" => 7
            ],
        ]);
    }
}
