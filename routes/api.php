<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\InventoryCategoryController;
use App\Http\Controllers\API\InventoryController;
use App\Http\Controllers\API\InventoryLocationController;
use App\Http\Controllers\API\TransactionController;
use App\Http\Controllers\API\TransactionDetailController;
use App\Http\Controllers\API\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1/auth'], function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('forgot', [AuthController::class, 'forgot'])->name('forgot');
    Route::post('reset', [AuthController::class, 'reset'])->name('reset');
    Route::post('register', [AuthController::class, 'register'])->name('register');
});
Route::group(['middleware' => ['auth:api', 'verified'], 'prefix' => 'v1'], function () {
    Route::get('logout', [AuthController::class, 'logout']);
});
Route::group(['middleware' => ['auth:api', 'verified', 'secureHeader'], 'prefix' => 'v1'], function () {
    Route::get('category', [InventoryCategoryController::class, 'get']);
    Route::post('category', [InventoryCategoryController::class, 'store']);
    Route::post('category/retrieve', [InventoryCategoryController::class, 'retrieve']);
    Route::put('category', [InventoryCategoryController::class, 'update']);
    Route::delete('category', [InventoryCategoryController::class, 'delete']);

    Route::get('location', [InventoryLocationController::class, 'get']);
    Route::post('location', [InventoryLocationController::class, 'store']);
    Route::post('location/retrieve', [InventoryLocationController::class, 'retrieve']);
    Route::put('location', [InventoryLocationController::class, 'update']);
    Route::delete('location', [InventoryLocationController::class, 'delete']);

    Route::get('inventory', [InventoryController::class, 'get']);
    Route::post('inventory', [InventoryController::class, 'store']);
    Route::post('inventory/retrieve', [InventoryController::class, 'retrieve']);
    Route::put('inventory', [InventoryController::class, 'update']);
    Route::delete('inventory', [InventoryController::class, 'delete']);

    Route::get('transaction', [TransactionController::class, 'get']);
    Route::post('transaction', [TransactionController::class, 'store']);
    Route::post('transaction/retrieve', [TransactionController::class, 'retrieve']);
    Route::put('transaction', [TransactionController::class, 'update']);
    Route::delete('transaction', [TransactionController::class, 'delete']);
    Route::get('transaction/detail', [TransactionDetailController::class, 'get']);
    Route::put('transaction/return', [TransactionDetailController::class, 'update']);
    Route::delete('transaction/number', [TransactionController::class, 'deleteByTransactionNumber']);

    Route::put('user/change-password', [UserController::class, 'changePassword']);
    Route::put('user', [UserController::class, 'update']);
});
