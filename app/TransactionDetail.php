<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Inventory;
class TransactionDetail extends Model
{
    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }
}
