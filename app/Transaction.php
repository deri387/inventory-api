<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Transaction extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->subject_id = Auth::user()->subject_id;
        });
    }
    public function details()
    {
        return $this->hasMany('App\TransactionDetail', 'transaction_id', 'id');
    }
}
