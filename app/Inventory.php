<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\InventoryCategory;
use App\InventoryLocation;

class Inventory extends Model
{
    protected $appends = ['category', 'location'];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->subject_id = Auth::user()->subject_id;
        });
    }

    function getCategoryAttribute() {
        return InventoryCategory::where("id", $this->attributes['inventory_category_id'])->first();
    }

    function getLocationAttribute() {
        return InventoryLocation::where("id", $this->attributes['inventory_location_id'])->first();
    }
}
