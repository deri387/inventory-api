<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Subject;
class Users extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasApiTokens;
    protected $fillable = [
        'name',
        'password',
        'phone',
        'status',
        'token',
    ];
    protected $appends = ['subject'];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = \Carbon\Carbon::now();
        });
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function getSubjectAttribute() {
        return Subject::where("id", $this->attributes['subject_id'])->first();
    }
}
