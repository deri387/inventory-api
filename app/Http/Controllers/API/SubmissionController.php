<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Submission;
use App\SubmissionAdditional;
use App\SubmissionBudget;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SubmissionController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required',
            'start' => 'required',
            'end' => 'required',
            'type' => 'required',
            'purpose' => 'required'
        ]);
    }

    public function get(Request $request)
    {
        try {
            $title = $request->query('title');
            $start = $request->query('start');
            $end = $request->query('end');
            $type = $request->query('type');
            $status = $request->query('status');
            $dataSubmission = Submission::when($title, function ($query) use ($title) {
                                        return $query->where('title', 'like', '%' . $title . '%');
                                    })->when($start && $end, function ($query) use ($start, $end) {
                                        return $query->whereRaw('(created_at >= ? AND created_at <= ?)', [$start." 00:00:00", $end." 23:59:59"]);
                                    })->when($type, function ($query) use ($type) {
                                        return $query->where('submission_type_id', $type);
                                    })->when($status, function ($query) use ($status) {
                                        return $query->where('status', $status);
                                    })->when(Auth::user()->role === 1, function ($query) {
                                        return $query->where('user_id', Auth::user()->id);
                                    })->orderBy("created_at", "desc")
                                    ->paginate($request->pageSize);
            return response()->json([
                'message' => '',
                'serve' => $dataSubmission,
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function store(Request $request)
    {
        DB::beginSubmission();
        try {
            $validate = $this->validator($request->all());
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataSubmission = new Submission;
            $dataSubmission->transaction_number = $request->transaction_number;
            $dataSubmission->name = $request->name;
            $dataSubmission->contact = $request->contact;
            $dataSubmission->user_id = Auth::user()->id;
            $dataSubmission->save();
            
            DB::commit();
            return response()->json([
                'message' => '',
                'serve' => [],
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function retrieve(Request $request)
    {
        try {
            $dataSubmission = Submission::where("id", $request->id)->first();
            if (!$dataSubmission) {
                return response()->json([
                    'message' => 'Data tidak diketahui.',
                    'serve' => []
                ], 400);
            }

            return response()->json([
                'message' => '',
                'serve' => $dataSubmission,
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function update(Request $request)
    {
        DB::beginSubmission();
        try {
            $validate = $this->validatorUpdate($request->all());
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataSubmission = Submission::where('transaction_number', $request->transaction_number)->first();
            if (!$dataSubmission) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal mendapatkan data.",
                    'serve' => []
                ], 400);
            }

            foreach($request->products as $product) {
                $dataProduct = Inventory::where("id", $product['inventory_id'])->first();
                if ($dataProduct) {
                    if ($dataProduct->qty < $product['qty']) {
                        return response()->json([
                            'message' => 'Stok produk '.$dataProduct->name.' kurang dari permintaan',
                            'serve' => $dataSubmission,
                        ], 400);
                    }

                    $dataProduct->qty = $dataProduct->qty - $product['qty'];
                    $dataProduct->save();
                }
                
                $dataSubmissionDetail = new SubmissionDetail();
                $dataSubmissionDetail->transaction_id = $dataSubmission->id;
                $dataSubmissionDetail->inventory_id = $product['inventory_id'];
                $dataSubmissionDetail->qty = $product['qty'];
                $dataSubmissionDetail->status = $product['status'];
                $dataSubmissionDetail->save();
            }

            DB::commit();
            return response()->json([
                'message' => 'Data berhasil disimpan.',
                'serve' => $dataSubmission,
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function delete(Request $request)
    {
        DB::beginSubmission();
        try {
            $dataSubmission = Submission::where("id", $request->id)->first();
            if (!$dataSubmission) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal mendapatkan data.",
                    'serve' => []
                ], 400);
            }

            $dataSubmission->delete();
            DB::commit();
            return response()->json([
                'message' => 'Data berhasil dihapus.',
                'serve' => [],
            ], 200);
        }  catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function deleteBySubmissionNumber(Request $request)
    {
        DB::beginSubmission();
        try {
            $dataSubmission = Submission::where('transaction_number', $request->transaction_number)->first();
            if (!$dataSubmission) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal menghapus data, karena data tidak valid.",
                    'serve' => []
                ], 400);
            }

            $dataSubmission->delete();
            DB::commit();
            return response()->json([
                'message' => 'Data berhasil dihapus.',
                'serve' => [],
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }
}
