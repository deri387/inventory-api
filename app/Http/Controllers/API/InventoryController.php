<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Inventory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InventoryController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string',
        ]);
    }

    public function get(Request $request)
    {
        try {
            $name = $request->query('name');
            $category = $request->query('category');
            $location = $request->query('location');
            $condition = $request->query('condition');
            $dataInventory = Inventory::when($name, function ($query) use ($name) {
                return $query->where('name', 'like', '%' . $name . '%');
            })->when($category, function ($query) use ($category) {
                return $query->where('inventory_category_id', $category);
            })->when($location, function ($query) use ($location) {
                return $query->where('inventory_location_id', $location);
            })->when($condition, function ($query) use ($condition) {
                return $query->where('condition', $condition);
            })->where("subject_id", Auth::user()->subject_id)
            ->orderBy("created_at", "desc")->paginate($request->pageSize);
            return response()->json([
                'message' => '',
                'serve' => $dataInventory,
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = $this->validator($request->all());
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataInventory = new Inventory;
            $dataInventory->inventory_category_id = $request->inventory_category_id;
            $dataInventory->inventory_location_id = $request->inventory_location_id;
            $dataInventory->name = $request->name;
            $dataInventory->qty = $request->qty;
            $dataInventory->condition = $request->condition;
            $dataInventory->condition_description = $request->condition_description;
            $dataInventory->save();

            DB::commit();
            return response()->json([
                'message' => 'Data baru berhasil ditambahkan.',
                'serve' => [],
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function retrieve(Request $request)
    {
        try {
            $dataInventory = Inventory::where("id", $request->id)->first();
            if (!$dataInventory) {
                return response()->json([
                    'message' => 'Data tidak diketahui.',
                    'serve' => []
                ], 400);
            }

            return response()->json([
                'message' => '',
                'serve' => $dataInventory,
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = $this->validator($request->all());
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataInventory = Inventory::where('id', $request->id)->first();
            if (!$dataInventory) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal mendapatkan data.",
                    'serve' => []
                ], 400);
            }

            $dataInventory->inventory_category_id = $request->inventory_category_id;
            $dataInventory->inventory_location_id = $request->inventory_location_id;
            $dataInventory->name = $request->name;
            $dataInventory->qty = $request->qty;
            $dataInventory->condition = $request->condition;
            $dataInventory->condition_description = $request->condition_description;
            $dataInventory->save();
            DB::commit();
            return response()->json([
                'message' => 'Data berhasil diubah.',
                'serve' => $dataInventory,
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {
            $dataInventory = Inventory::where('id', $request->id)->first();
            if (!$dataInventory) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal mendapatkan data.",
                    'serve' => []
                ], 400);
            }

            $dataInventory->delete();
            DB::commit();
            return response()->json([
                'message' => 'Data berhasil dihapus.',
                'serve' => [],
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }
}
