<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Transaction;
use App\TransactionDetail;
use App\Inventory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'transaction_number' => 'required|unique:transactions',
        ]);
    }

    protected function validatorUpdate(array $data)
    {
        return Validator::make($data, [
            'transaction_number' => 'required',
            'products' => 'required',
        ]);
    }

    public function get(Request $request)
    {
        try {
            $transaction_number = $request->query('transaction_number');
            $start = $request->query('start');
            $end = $request->query('end');
            $dataTransaction = Transaction::with('details.inventory')
                                    ->when($transaction_number, function ($query) use ($transaction_number) {
                                        return $query->where('transaction_number', $transaction_number);
                                    })->when($start && $end, function ($query) use ($start, $end) {
                                        return $query->whereRaw('(created_at >= ? AND created_at <= ?)', [$start." 00:00:00", $end." 23:59:59"]);
                                    })->where("subject_id", Auth::user()->subject_id)
                                    ->orderBy("created_at", "desc")
                                    ->paginate($request->pageSize);
            return response()->json([
                'message' => '',
                'serve' => $dataTransaction,
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = $this->validator($request->all());
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataTransaction = new Transaction;
            $dataTransaction->transaction_number = $request->transaction_number;
            $dataTransaction->name = $request->name;
            $dataTransaction->contact = $request->contact;
            $dataTransaction->user_id = Auth::user()->id;
            $dataTransaction->save();
            
            DB::commit();
            return response()->json([
                'message' => '',
                'serve' => [],
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function retrieve(Request $request)
    {
        try {
            $dataTransaction = Transaction::where("id", $request->id)->first();
            if (!$dataTransaction) {
                return response()->json([
                    'message' => 'Data tidak diketahui.',
                    'serve' => []
                ], 400);
            }

            return response()->json([
                'message' => '',
                'serve' => $dataTransaction,
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = $this->validatorUpdate($request->all());
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataTransaction = Transaction::where('transaction_number', $request->transaction_number)->first();
            if (!$dataTransaction) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal mendapatkan data.",
                    'serve' => []
                ], 400);
            }

            foreach($request->products as $product) {
                $dataProduct = Inventory::where("id", $product['inventory_id'])->first();
                if ($dataProduct) {
                    if ($dataProduct->qty < $product['qty']) {
                        return response()->json([
                            'message' => 'Stok produk '.$dataProduct->name.' kurang dari permintaan',
                            'serve' => $dataTransaction,
                        ], 400);
                    }

                    $dataProduct->qty = $dataProduct->qty - $product['qty'];
                    $dataProduct->save();
                }
                
                $dataTransactionDetail = new TransactionDetail();
                $dataTransactionDetail->transaction_id = $dataTransaction->id;
                $dataTransactionDetail->inventory_id = $product['inventory_id'];
                $dataTransactionDetail->qty = $product['qty'];
                $dataTransactionDetail->status = $product['status'];
                $dataTransactionDetail->save();
            }

            DB::commit();
            return response()->json([
                'message' => 'Data berhasil disimpan.',
                'serve' => $dataTransaction,
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {
            $dataTransaction = Transaction::where("id", $request->id)->first();
            if (!$dataTransaction) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal mendapatkan data.",
                    'serve' => []
                ], 400);
            }

            $dataTransaction->delete();
            DB::commit();
            return response()->json([
                'message' => 'Data berhasil dihapus.',
                'serve' => [],
            ], 200);
        }  catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function deleteByTransactionNumber(Request $request)
    {
        DB::beginTransaction();
        try {
            $dataTransaction = Transaction::where('transaction_number', $request->transaction_number)->first();
            if (!$dataTransaction) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal menghapus data, karena data tidak valid.",
                    'serve' => []
                ], 400);
            }

            $dataTransaction->delete();
            DB::commit();
            return response()->json([
                'message' => 'Data berhasil dihapus.',
                'serve' => [],
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }
}
