<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Inventory;
use App\TransactionDetail;
use Illuminate\Support\Facades\DB;

class TransactionDetailController extends Controller
{
    public function get(Request $request)
    {
        try {
            $id = $request->query('id');
            $dataTransactionDetail = TransactionDetail::with("inventory")->when($id, function ($query) use ($id) {
                return $query->where('transaction_id', $id);
            })->orderBy("created_at", "desc")->get();
            return response()->json([
                'message' => '',
                'serve' => $dataTransactionDetail,
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $dataTransactionDetail = TransactionDetail::where('id', $request->id)->first();
            if (!$dataTransactionDetail) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal mendapatkan data.",
                    'serve' => []
                ], 400);
            }

            $dataInventory = Inventory::where("id", $dataTransactionDetail->inventory_id)->first();
            if (!$dataInventory) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal mendapatkan data.",
                    'serve' => []
                ], 400);
            }

            $dataInventory->qty = $dataInventory->qty + $dataTransactionDetail->qty;
            $dataInventory->save();
            
            $dataTransactionDetail->status = 2;
            $dataTransactionDetail->save();
            DB::commit();
            return response()->json([
                'message' => 'Data berhasil dikembalikan.',
                'serve' => $dataTransactionDetail,
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }
}
