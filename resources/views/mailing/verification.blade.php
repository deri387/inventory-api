@extends('layouts.app')
@section('title', 'Verifikasi Email')
@section('meta-title', 'Verifikasi email Simple School')
@section('meta-description', 'Verifikasi email anda untuk dapat mengakses fitur-fitur di simpleschooldev.com')
@section('content')
<div class="bg-grey">
    <div class="container">
        <div class="d-flex justify-content-center w-100">
            <img src="{{ asset('images/logo.png') }}" class="img-responsive pt-3" width="100">
        </div>
        <div class="pt-3">
            <div class="card p-5">
                <h5 class="font-weight-bold">Hello {{ $data->full_name }},</h5>
                <p>Terima kasih anda telah bergabung bersama kami. Klik button dibawah ini untuk memverifikasi email
                    anda.</p>
                <a class="btn btn-primary btn-sm m-auto" href="{{ $url }}">Konfirmasi email</a>
                <p class="mt-3">Jika anda tidak merasa membuat akun, abaikan pesan ini.</p>
                <p>Regards,</p>
                <p class="font-weight-bold">Simple School</p>
            </div>
            <footer class="fs-12 text-center mt-3 mb-3">&copy; {{ date("Y") }} Simple School. All Right Reserved</footer>
        </div>
    </div>
</div>
@endsection
