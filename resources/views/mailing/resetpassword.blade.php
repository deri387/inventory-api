@extends('layouts.app')
@section('title', 'Reset Password')
@section('meta-title', 'Reset Password Simple School')
@section('meta-description', 'Reset password di simpleschooldev.com')
@section('content')
<div class="bg-grey">
    <div class="container">
        <div class="d-flex justify-content-center w-100">
            <img src="{{ asset('images/logo.png') }}" class="img-responsive pt-3" width="100">
        </div>
        <div class="pt-3">
            <div class="card p-5">
                <h5 class="font-weight-bold">Hello !</h5>
                <p>Kami mendengar bahwa anda ingin melakukan reset password. Harap klik button dibawah ini untuk melakukan reset password. Link ini hanya berlaku 1 kali.</p>
                <a class="btn btn-primary btn-sm m-auto" href="{{ $url }}">Reset Password</a>
                <p class="mt-3">Jika anda merasa tidak melakukan reset password, abaikan pesan ini.</p>
                <p>Regards,</p>
                <p class="font-weight-bold">Simple School</p>
            </div>
            <footer class="fs-12 text-center mt-3 mb-3">&copy; {{ date("Y") }} Simple School. All Right Reserved</footer>
        </div>
    </div>
</div>
@endsection
