@extends('layouts.app')
@section('title', 'General Email')
@section('meta-title', 'General Email Simple School')
@section('meta-description', 'General email di simpleschooldev.com')
@section('content')
<div class="bg-grey">
    <div class="container">
        <div class="d-flex justify-content-center w-100">
            <img src="{{ asset('images/logo.png') }}" class="img-responsive pt-3" width="100">
        </div>
        <div class="pt-3">
            <div class="card p-5">
                {!! $content !!}
                <p>Regards,</p>
                <p class="font-weight-bold">Simple School</p>
            </div>
            <footer class="fs-12 text-center mt-3 mb-3">&copy; {{ date("Y") }} Simple School. All Right Reserved</footer>
        </div>
    </div>
</div>
@endsection
