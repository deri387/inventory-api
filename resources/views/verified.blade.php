@extends('layouts.app')
@section('title', 'Verifikasi Berhasil')
@section('meta-title', 'Verifikasi email Simple School')
@section('meta-description', 'Verifikasi email anda untuk dapat mengakses fitur-fitur di simpleschool.ahadcreativestudio.com')
@section('content')
<div class="bg-grey">
    <div class="container">
        <div class="d-flex justify-content-center w-100">
            <img src="{{ asset('images/logo.png') }}" class="img-responsive pt-3" width="100">
        </div>
        <div class="pt-3">
            <div class="card p-5">
                <div class="row align-items-center">
                    <div class="col-md-3">
                    <img src="{{ asset('images/verified.png') }}" class="img-responsive pt-3 w-100">
                    </div>
                    <div class="col-md-9">
                        <h5 class="font-weight-bold color-1">Selamat email berhasil terverifikasi!</h5>
                        <p>Sekarang anda dapat mengakses fitur-fitur yang ada di simpleschool.ahadcreativestudio.com, klik button
                            dibawah untuk langsung mengarahkan ke halaman login</p>
                        <a href="{{ env('APP_CLIENT') }}" class="btn btn-primary btn-sm mr-auto">Login sekarang</a>
                    </div>
                </div>
            </div>
            <footer class="fs-12 text-center mt-3 mb-3">&copy; {{ date("Y") }} Simple School. All Right Reserved</footer>
        </div>
    </div>
</div>
@endsection
