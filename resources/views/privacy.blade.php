@extends('layouts.app')
@section('title', 'Simple School - Website CMS Sekolah Terbaru di Indonesia')
@section('meta-title', 'Simple School - Website CMS Sekolah Terbaru di Indonesia')
@section('meta-description', 'Buat website sekolahmu hanya dalam 5 menit dengan Simple School yaitu website CMS sekolah
pertama di Indonesia')
@section('content')
<div class="container">
    <h3 class="text-title text-center mt-5 mb-4">Privacy and Policy</h3>
    <p class="text-green">
        Privasi Anda penting bagi kami. Ini adalah kebijakan Simple School untuk menghormati privasi Anda
        mengenai segala informasi yang kami dapat dari Anda di situs web kami, <a target="_blank"
            href="http://simpleschool.ahadcreativestudi.com">http://simpleschool.ahadcreativestudio.com</a>, dan situs lain yang kami miliki
        dan operasikan. <br><br>

        Kami hanya meminta informasi pribadi ketika kami benar-benar membutuhkannya untuk memberikan layanan
        kepada Anda. Kami mengumpulkannya dengan cara yang adil dan sah, dengan sepengetahuan dan persetujuan
        Anda. Kami juga memberi tahu Anda mengapa kami mengumpulkannya dan bagaimana menggunakannya. <br><br>

        Kami hanya menyimpan informasi yang dikumpulkan selama diperlukan untuk menyediakan layanan yang Anda
        minta. Data yang kami simpan, akan kami lindungi dengan cara yang dapat diterima secara komersial untuk
        mencegah kehilangan dan pencurian, serta akses, pengungkapan, penyalinan, penggunaan, atau modifikasi
        yang tidak sah. <br><br>

        Kami tidak membagikan informasi pengenal pribadi apa pun secara publik atau dengan pihak ketiga, kecuali
        jika diharuskan oleh hukum. <br><br>

        Situs web kami dapat terhubung ke situs eksternal yang tidak dioperasikan oleh kami. Perlu diketahui
        bahwa kami tidak memiliki kontrol atas konten dan praktik situs ini, dan tidak dapat menerima tanggung
        jawab atau kewajiban untuk kebijakan privasi masing-masing. <br><br>

        Anda bebas untuk menolak permintaan kami untuk informasi pribadi Anda, dengan pengertian bahwa kami
        mungkin tidak dapat memberikan Anda beberapa layanan yang Anda inginkan. <br><br>

        Saat menggunakan layanan kami akan dianggap sebagai penerimaan praktik kami seputar privasi dan
        informasi pribadi. Jika Anda memiliki pertanyaan tentang bagaimana kami menangani data pengguna dan
        informasi pribadi, jangan ragu untuk menghubungi kami. <br><br>

        Kebijakan ini berlaku mulai 15 Oktober 2019.
    </p>
</div>
</div>
@endsection
