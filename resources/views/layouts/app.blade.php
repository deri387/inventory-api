<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
    <meta name="keywords"
        content="buat website sekolah, cms web sekolah gratis, web sekolah gratis, cms web sekolah, buat website sekoalah, CMS, Website Sekolah Gratis, Cara Membuat Website Sekolah, membuat web sekolah, contoh website sekolah, fitur website sekolah, Sekolah, Website, Internet,Situs, CMS Sekolah, Web Sekolah, Website Sekolah Gratis, Website Sekolah, Aplikasi Sekolah, PPDB Online, PSB Online, PSB Online Gratis, Penerimaan Siswa Baru Online, Raport Online, Kurikulum 2013, SD, SMP, SMA, Aliyah, MTs, SMK, CMS Indonesia" />
    <meta name="subject" content="Situs Pendidikan" />
    <meta name="copyright" content="Simple School" />
    <meta name="language" content="Indonesia" />
    <meta name="robots" content="index,follow" />
    <meta name="Classification" content="Education" />
    <meta name="author" content="Deri Komara, komaraderii@gmail.com" />
    <meta name="designer" content="Deri Komara, komaraderii@gmail.com" />
    <meta name="reply-to" content="komaraderii@gmail.com" />
    <meta name="owner" content="Deri Komara">
    <meta name="description" content="@yield('meta-description')">
    <meta property="og:url" content="{{ env('APP_URL') }}" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta property="twitter:url" content="{{ env('APP_URL') }}" />
    <meta property="og:type" content="website" />
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta property="og:title" content="@yield('meta-title')" />
    <meta property="og:description" content="@yield('meta-description')" />
    <meta property="twitter:title" content="@yield('meta-title')" />
    <meta property="twitter:description" content="@yield('meta-description')" />
    <link rel="manifest" href="{{ asset('site.webmanifest') }}">
    <meta name="msapplication-TileColor" content="#ff9933">
    <meta name="theme-color" content="#ff9933">
    <title>@yield('title') ~ Simple School</title>
    <link rel="shortcut icon" href="{{ asset('images/icon.png') }}">
    <link rel="canonical" href="{{ env('APP_URL') }}" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel='stylesheet' href="{{ asset('css/all.min.css') }}" type='text/css' media='all' />
    <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}" type='text/css' media='all'>
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}" type='text/css' media='all'>
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.min.css') }}" type='text/css' media='all'>
    <link rel="stylesheet" href="{{ asset('css/perfect-scrollbar.css') }}" type='text/css' media='all'>
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}" type='text/css' media='all'>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type='text/css' media='all'>
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type='text/css' media='all'>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/60ae1e19de99a4282a19a5a0/1f6k2b5rv';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();

    </script>
    <!--End of Tawk.to Script-->
</head>

<body>
    @yield('content')
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('js/popper.min.js') }}"></script>

    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script src="{{ asset('js/counterup.min.js') }}"></script>
    <script src="{{ asset('js/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('js/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script src="{{ asset('js/custom-scripts.js') }}"></script>
</body>

</html>
