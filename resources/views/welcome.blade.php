@extends('layouts.app')
@section('title', 'Simple School - Website CMS Sekolah Terbaru di Indonesia')
@section('meta-title', 'Simple School - Website CMS Sekolah Terbaru di Indonesia')
@section('meta-description', 'Buat website sekolahmu hanya dalam 5 menit dengan Simple School yaitu website CMS sekolah
pertama di Indonesia')
@section('content')
<main>
    <header class="style4 bg-color1 d-flex flex-wrap align-items-center w-100 fixed-top">
        <div class="logo bg-color1 position-relative">
            <h1 class="mb-0"><a class="d-block" href="/" title="Home"><img width="150" class="img-fluid"
                        src="{{ asset('images/logo.png') }}" alt="Logo"></a></h1>
        </div><!-- Logo -->
        <div class="topbar-menu-wrap">
            <div
                class="quote-menu-wrap d-flex flex-wrap align-items-center justify-content-end position-relative w-100 bg-color1">
                <div class="nav-btns-search-wrap d-inline-flex flex-wrap align-items-center justify-content-between">
                    <nav class="d-flex flex-wrap align-items-center">
                        <ul class="d-flex flex-wrap align-items-center mb-0 list-unstyled w-100">
                            <li><a href="#intro" title="#intro">Intro</a></li>
                            <li><a href="#layanan" title="#layanan">Layanan</a></li>
                            <li><a href="#cara-kerja" title="#cara-kerja">Cara Kerja</a></li>
                            <li><a href="#harga" title="#harga">Harga</a></li>
                            <li><a href="#kontak" title="#kontak">Kontak</a></li>
                        </ul>
                    </nav>
                    <div class="btn-search d-inline-flex align-items-center">
                        <a class="thm-btn p-2 grad-bg1 brd-rd5 d-inline-block overflow-hidden position-relative text-center"
                            href="{{ env('APP_CLIENT') }}" title="" style="font-size: 13px;">Coba Sekarang</a>
                    </div>
                </div><!-- Navigation, Button & Search Wrap -->
            </div><!-- Quote & Menu Wrap -->
        </div>
    </header><!-- Header -->
    <div class="responsive-header position-relative w-100 position-fixed">
        <div class="res-logo-search-bar bg-color1 w-100">
            <div class="container">
                <div
                    class="res-logo-search-bar-inner d-flex flex-wrap justify-content-between align-items-center w-100">
                    <div class="logo">
                        <h1 class="mb-0"><a href="/" title="Home"><img class="img-fluid" src="assets/images/logo.png"
                                    alt="Logo"></a></h1>
                    </div><!-- Logo -->
                    <div class="res-btns d-inline-flex flex-wrap">
                        <a class="res-menu-btn brd-rd5 d-inline-block" href="javascript:void(0);" title=""><i
                                class="icon-menu"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="res-menu d-flex flex-wrap justify-content-center w-100">
            <div class="res-menu-inner">
                <ul class="mb-0 list-unstyled">
                    <li><a href="#intro" title="#intro">Intro</a></li>
                    <li><a href="#layanan" title="#layanan">Layanan</a></li>
                    <li><a href="#cara-kerja" title="#cara-kerja">Cara Kerja</a></li>
                    <li><a href="#harga" title="#harga">Harga</a></li>
                    <li><a href="#kontak" title="#kontak">Kontak</a></li>
                </ul>
                <a class="thm-btn brd-btn invr brd-rd5 d-inline-block text-center overflow-hidden position-relative"
                    href="{{ env('APP_CLIENT') }}" title="">Coba Sekarang</a>
            </div>
        </div>
    </div>
    <section class="mt-5">
        <div class="w-100 position-relative pt-50">
            <div class="feat-wrap4 pt-130 pb-170 overflow-hidden position-relative w-100">
                <img height="792" width="1074" class="img-fluid rgt-tp-shp position-absolute"
                    src="assets/images/resources/feat-area-mckp.png" alt="Featured Area Mockup">
                <div class="container">
                    <div class="feat-cap4 position-relative w-100">
                        <span class="d-block text-color1">CMS Web Sekolah Terbaru dan Termudah</span>
                        <h2 class="mb-0">Solusi Pembuatan Web Sekolah Bagi Anda</h2>
                        <p class="mb-0">Kami selalu berikan hasil yang terbaik kepada sekolah yang mendaftar dengan kami
                            sejak tahun 2019</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mt-5">
        <div class="w-100 pt-60 position-relative">
            <div class="container">
                <div class="about-wrap4 overlap-170 position-relative w-100">
                    <div class="row mrg30">
                        <div class="col-md-12 col-sm-12 col-lg-12 order-lg-1">
                            <div class="about-img4 text-center position-relative">
                                <div class="about-img-inner4 d-inline-block position-relative">
                                    <img height="434" width="970" class="img-fluid brd-rd10 w-100"
                                        src="assets/images/resources/about-img3.jpg" alt="About Image 3">
                                    <div class="exp-box brd-rd10 position-absolute bg-color1">
                                        <h3 class="mb-0"><span class="d-block">Preview Web</span></h3>
                                        <p class="mb-0">SMKN 1 Katapang</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-lg-6" id="intro">
                            <div class="about-title-wrap w-100">
                                <div class="sec-title mb-0 position-relative w-100">
                                    <div class="sec-title-inner d-inline-block position-relative">
                                        <span class="d-block text-color1">INTRO</span>
                                        <h3 class="mb-0">CMS Simpleschool</h3>
                                        <p class="mb-0">Mempermudah anda dalam pembuatan website sekolah hanya dalam
                                            beberapa langkah</p>
                                    </div>
                                </div><!-- Sec Title -->
                                <a class="thm-btn mt-3 grad-bg1 brd-rd5 d-inline-block overflow-hidden position-relative text-center"
                                    href="{{ env('APP_CLIENT') }}" title="">Coba Sekarang</a>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-lg-6">
                            <div class="about-desc4 w-100">
                                <h4 class="mb-0 text-color1">Selama bertahun-tahun, berbagai perkembangan dan inovasi
                                    pada teknologi berhubungan dengan sekolah</h4>
                                <p class="mb-0">Kami selalu memperbarui sistem agar tetap menjaga kestabilan dengan
                                    teknologi yang sedang tren dan tentunya akan memberikan dampak sangat baik kepada
                                    client kami</p>
                                <div class="mini-fun-facts-wrap w-100">
                                    <div class="row mrg30">
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="fact-box3 w-100">
                                                <h3 class="mb-0 text-color1"><span
                                                        class="counter">2</span><small>thn</small></h3>
                                                <p class="mb-0">Pengalaman</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="fact-box3 w-100">
                                                <h3 class="mb-0 text-color1"><span
                                                        class="counter">24</span><small>jam</small></h3>
                                                <p class="mb-0">Bantuan Masalah</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="fact-box3 w-100">
                                                <h3 class="mb-0 text-color1"><span
                                                        class="counter">10</span><small>+</small></h3>
                                                <p class="mb-0">Client Terdaftar </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- About Wrap 4 -->
            </div>
        </div>
    </section>
    <section>
        <div class="w-100 pt-280 bg-color6 pb-110 position-relative">
            <img height="170" width="1920" class="img-fluid lft-tp-shp position-absolute w-100"
                src="assets/images/shap8.png" alt="Shape">
            <div class="container">
                <div class="sec-title text-center position-relative w-100" id="layanan">
                    <div class="sec-title-inner d-inline-block position-relative">
                        <span class="d-block text-color1">LAYANAN</span>
                        <h3 class="mb-0">Berbagai Layanan Yang <br /> Dapat Mempermudah Anda</h3>
                        <p class="mb-0">Layanan yang kami berikan sesuai dengan apa yang sekolah butuhkan </p>
                    </div>
                </div><!-- Sec Title -->
                <div class="we-offer-srv-wrap res-row text-center position-relative w-100">
                    <div class="row align-items-center mrg30">
                        <div class="col-md-6 col-sm-6 col-lg-4">
                            <div class="we-offer-srv-box brd-rd20 text-center w-100">
                                <span class="d-inline-block text-color1"><i class="icon-web-window"></i></span>
                                <h3 class="mb-0">Content Management System</h3>
                                <p class="mb-0">Sekolah dapat mengatur konten website dan memilih template website yang
                                    diinginkan</p>
                                <!-- <a class="d-inline-block text-color1" href="javascript:void(0);" title="">Read More</a> -->
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-4">
                            <div class="we-offer-srv-box brd-rd20 text-center w-100">
                                <span class="d-inline-block text-color1"><i class="icon-call2"></i></span>
                                <h3 class="mb-0">Contact Management System</h3>
                                <p class="mb-0">Tersedia contact website maupun live chat bila ingin berinteraksi dengan
                                    visitor</p>
                                <!-- <a class="d-inline-block text-color1" href="javascript:void(0);" title="">Read More</a> -->
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-4">
                            <div class="we-offer-srv-box brd-rd20 text-center w-100">
                                <span class="d-inline-block text-color1"><i class="icon-cyber-security"></i></span>
                                <h3 class="mb-0">Security Management System</h3>
                                <p class="mb-0">Dengan keamanan yang kami terapkan, website aman dari serangan cyber</p>
                                <!-- <a class="d-inline-block text-color1" href="javascript:void(0);" title="">Read More</a> -->
                            </div>
                        </div>
                    </div>
                </div><!-- We Offer Services Wrap -->
                <div class="all-btn mt-70 d-inline-block position-relative text-center w-100">
                    <a class="thm-btn grad-bg1 brd-rd5 d-inline-block overflow-hidden position-relative text-center"
                        href="{{ env('APP_CLIENT') }}" title="">Coba Sekarang</a>
                </div><!-- All Button -->
            </div>
        </div>
    </section>
    <section>
        <div class="w-100 pt-100 pb-120 position-relative">
            <div class="container">
                <div class="about-wrap2 position-relative w-100">
                    <div class="row align-items-center mrg30">
                        <div class="col-md-12 col-sm-12 col-lg-6 order-lg-1">
                            <div class="about-img position-relative text-center text-lg-end w-100">
                                <img height="528" width="507" class="img-fluid"
                                    src="assets/images/resources/about-mckp-img5.png" alt="About Mockup Image 5">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-lg-6" id="cara-kerja">
                            <div class="about-desc w-100">
                                <span class="d-block text-color1">CARA KERJA</span>
                                <h3 class="mb-0">Tentang Cara Kerja Simpleschool</h3>
                                <p class="mb-0">Simpleschool merupakan CMS siap pakai alias sekolah tidak perlu
                                    menyiapkan kebutuhan-kebutuhan seperti Server ataupun yang lainnya, namun
                                    Simpleschool memberikan kebebasan pengelolaan pada admin disetiap sekolah yang telah
                                    terdaftar sebagai member Simpleschool. Pada panel atau halaman admin, setiap
                                    sekolah memiliki hak akses mengelola datanya, 1 panel atau halaman admin untuk semua
                                    sistem yang ada di Simpleschool.
                                    <br /><br />

                                    Simpleschool pun akan senantiasa memberikan bantuan 24/7 jika ada sekolah yang
                                    memiliki kesulitan.</p>
                            </div>
                        </div>
                    </div>
                </div><!-- About Wrap 2 -->
            </div>
        </div>
    </section>
    <section class="mb-5">
        <div class="w-100 pt-110 blue-layer2 opc95 position-relative">
            <div class="fixed-bg" style="background-image: url(assets/images/parallax-bg7.jpg);"></div>
            <img height="170" width="1920" class="img-fluid lft-btm-shp position-absolute w-100"
                src="assets/images/shap9.png" alt="Shape">
            <img height="484" width="1920" class="img-fluid lft-btm-shp position-absolute w-100"
                src="assets/images/wave-shap.png" alt="Wave Shape">
            <div class="container">
                <div class="sec-title text-center position-relative w-100" id="harga">
                    <div class="sec-title-inner d-inline-block position-relative">
                        <span class="d-block text-color1">HARGA</span>
                        <h3 class="mb-0">Kami tawarkan harga <br> yang cocok untuk anda</h3>
                    </div>
                </div><!-- Sec Title -->
                <div class="packages-wrap2 res-row overlap-140 position-relative w-100">
                    <div class="row mrg30">
                        <div class="col-md-6 col-sm-12 col-lg-4">
                            <div class="package-box2 brd-rd20 grad-bg1 text-center w-100">
                                <div class="package-head d-flex flex-wrap align-items-center w-100">
                                    <span class="brd-rd20 text-center"><img class="img-fluid"
                                            src="assets/images/package-icon1.svg" alt="Package Icon 1"></span>
                                    <strong>GRATIS<i class="d-block">SELAMANYA</i></strong>
                                </div>
                                <div class="package-body w-100">
                                    <h3 class="mb-0">Basic</h3>
                                    <ul class="list-style1 mb-0 list-unstyled w-100">
                                        <li>Limit konten ( blog, gallery, dll )</li>
                                        <li>1 Akun</li>
                                        <li>1 Tema</li>
                                        <!-- <li>Domain (.simpleschooldev.com)</li> -->
                                        <li>CS 24 Jam</li>
                                        <li>Lainnya</li>
                                    </ul>
                                    <a class="thm-btn brd-btn invr brd-rd5 d-inline-block overflow-hidden position-relative"
                                        href="{{ env('APP_CLIENT') }}" title="">Coba Sekarang</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-4">
                            <div class="package-box2 brd-rd20 grad-bg2 text-center w-100">
                                <div class="package-head d-flex flex-wrap align-items-center w-100">
                                    <span class="brd-rd20 text-center"><img class="img-fluid"
                                            src="assets/images/package-icon2.svg" alt="Package Icon 2"></span>
                                    <strong>Rp. 500k<i class="d-block">/thn</i></strong>
                                </div>
                                <div class="package-body w-100">
                                    <h3 class="mb-0">Standard</h3>
                                    <ul class="list-style1 mb-0 list-unstyled w-100">
                                        <li>Tanpa limit konten</li>
                                        <li>Include Domain (.sch.id, .com)</li>
                                        <li>1 Akun</li>
                                        <li>1 Tema</li>
                                        <!-- <li>Domain (.simpleschooldev.com)</li> -->
                                        <li>CS 24 Jam</li>
                                        <li>Lainnya</li>
                                    </ul>
                                    <a class="thm-btn brd-btn invr brd-rd5 d-inline-block overflow-hidden position-relative"
                                        href="http://wa.link/h6xq0x" title="">Pilih Sekarang</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-4">
                            <div class="package-box2 brd-rd20 grad-bg3 text-center w-100">
                                <div class="package-head d-flex flex-wrap align-items-center w-100">
                                    <span class="brd-rd20 text-center"><img class="img-fluid"
                                            src="assets/images/package-icon3.svg" alt="Package Icon 3"></span>
                                    <strong>Rp.1.5jt<i class="d-block">S&K Berlaku</i></strong>
                                </div>
                                <div class="package-body w-100">
                                    <h3 class="mb-0">Premium</h3>
                                    <ul class="list-style1 mb-0 list-unstyled w-100">
                                        <li>Tanpa limit konten</li>
                                        <li>Multi Akun</li>
                                        <li>Multi Tema</li>
                                        <li>Include Domain (.sch.id, .com)</li>
                                        <li>Custom Fitur</li>
                                        <li>Entry Data Support</li>
                                        <!-- <li>Domain (.simpleschooldev.com)</li> -->
                                        <li>CS 24 Jam</li>
                                        <li>Lainnya</li>
                                    </ul>
                                    <a class="thm-btn brd-btn invr brd-rd5 d-inline-block overflow-hidden position-relative"
                                        href="http://wa.link/h6ylry" title="">Pilih Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- Packages Wrap 2 -->
            </div>
        </div>
    </section>
    <section>
        <div class="w-100 pt-110 mt-5 position-relative">
            <div class="container">
                <div class="have-questions-wrap position-relative w-100">
                    <div class="row justify-content-between mrg30">
                        <div class="col-md-12 col-sm-12 col-lg-6" id="kontak">
                            <div class="sec-title position-relative w-100">
                                <div class="sec-title-inner d-inline-block position-relative">
                                    <span class="d-block text-color1">KONTAK</span>
                                    <h3 class="mb-0">Memiliki Pertanyaan? <br> <span>Kontak Kami</span> Segera!</h3>
                                    <p class="mb-0">Anda dapat menanyakan pertanyaan dengan cara menghubungi kontak yang
                                        tertera</p>
                                    <p class="mb-0">Telp : +6281902420670</p>
                                    <p class="mb-0">Email : komaraderii@gmail.com</p>
                                </div>
                            </div><!-- Sec Title -->
                        </div>
                        <div class="col-md-12 col-sm-12 col-lg-5">
                            <div class="req-wrap v2 brd-rd10 bg-color1 position-relative text-center w-100 z1">
                                <div class="req-title w-100">
                                    <h3 class="mb-0">Form Kontak</h3>
                                    <p class="mb-0">Ayo buat website sekolah sekarang!</p>
                                </div>
                                <form class="w-100">
                                    <div class="field-box position-relative w-100">
                                        <input class="w-100 brd-rd5" type="text" placeholder="Name*">
                                    </div>
                                    <div class="field-box position-relative w-100">
                                        <input class="w-100 brd-rd5" type="email" placeholder="E-mail Address*">
                                    </div>
                                    <div class="field-box position-relative w-100">
                                        <input class="w-100 brd-rd5" type="tel" placeholder="Phone*">
                                    </div>
                                    <div class="field-box position-relative w-100">
                                        <textarea class="w-100 brd-rd5" placeholder="Description"></textarea>
                                    </div>
                                    <div class="field-btn w-100">
                                        <button
                                            class="thm-btn grad-bg1 brd-rd5 d-inline-block overflow-hidden position-relative text-center w-100"
                                            type="submit">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- Have Questions Wrap -->
            </div>
            <div class="google-map v2 w-100">
                <iframe class="d-block"
                    src="https://www.google.com/maps/embed/v1/place?q=Distro+Programmer&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"
                    width="100%" height="525" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </section>
    <footer class="style3">
        <div class="w-100 pt-80 white-layer opc2 pb-110 position-relative">
            <div class="fixed-bg"></div>
            <div class="container">
                <div class="footer-data position-relative w-100">
                    <div class="row mrg30">
                        <div class="col-md-12 col-sm-12 col-lg-4">
                            <div class="widget w-100">
                                <h3 class="widget-title text-color1">Tentang Kami.</h3>
                                <p class="mb-0">Simpleschool merupakan CMS Web Sekolah yang mempermudah dalam pembuatan
                                    website sekolah</p>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-lg-4">
                            <div class="widget w-100">
                                <h3 class="widget-title text-color1">Offical info.</h3>
                                <ul class="cont-list v2 mb-0 list-unstyled w-100">
                                    <li><i class="fas fa-home text-color1"></i>Distro Programmer</li>
                                    <li><i class="fas fa-phone-alt text-color1"></i><a href="tel:+6281902420670"
                                            title="">+6281902420670</a></li>
                                    <li><i class="fas fa-envelope-open text-color1"></i><a
                                            href="mailto:komaraderii@gmail.com" title="">komaraderii@gmail.com</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-lg-4">
                            <div class="widget w-100">
                                <h3 class="widget-title text-color1">IT Services.</h3>
                                <ul class="v2 mb-0 list-unstyled">
                                    <li><a href="#layanan" title="">Content Management System</a></li>
                                    <li><a href="#layanan" title="">Contact Management System</a></li>
                                    <li><a href="#layanan" title="">Security Management System</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- Footer Data -->
            </div>
        </div>
    </footer><!-- Footer -->
    <div class="bottom-bar bg-color1 text-center position-relative w-100">
        <p class="mb-0">Copyright &copy; <?php echo date("Y"); ?> <a href="/" title="Simpleschool"
                target="_blank">Simpleschool</a>. All
            Rights Reserved.</p>
    </div><!-- Bottom Bar -->
</main><!-- Main Wrapper -->

@endsection
