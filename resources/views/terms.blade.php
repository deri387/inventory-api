@extends('layouts.app')
@section('title', 'Simple School - Website CMS Sekolah Terbaru di Indonesia')
@section('meta-title', 'Simple School - Website CMS Sekolah Terbaru di Indonesia')
@section('meta-description', 'Buat website sekolahmu hanya dalam 5 menit dengan Simple School yaitu website CMS sekolah
pertama di Indonesia')
@section('content')
<div class="content container">
    <h3 class="text-title text-center mt-5 mb-4">Terms & Conditions</h3>
    <ol class="text-green">
        <div class="item-1 mb-4 mt-5">
            <li>Ketentuan</li>
            <p class="text-justify">Dengan mengakses situs web di <a target="_blank"
                    href="http://simpleschool.ahadcreativestudio.com">http://simpleschool.ahadcreativestudio.com</a>, Anda setuju untuk terikat
                oleh persyaratan layanan ini, semua undang-undang dan peraturan yang berlaku, dan persetujuan
                bahwa Anda bertanggung jawab untuk kepatuhan terhadap hukum yang berlaku. Jika Anda tidak setuju
                dengan ketentuan ini, Anda dilarang menggunakan atau mengakses situs ini. Materi yang terkandung
                dalam situs web ini dilindungi oleh undang-undang hak cipta dan merek dagang yang berlaku.</p>
        </div>

        <div class="item-2 mb-4">
            <li>Lisensi</li>
            <ol>
                <li type="A">Izin diberikan untuk mengunduh sementara satu materi (informasi atau perangkat
                    lunak) sementara di situs web Simple School hanya untuk tampilan sementara, non-komersial.
                    Ini adalah pemberian lisensi, bukan transfer judul, dan di bawah lisensi ini Anda tidak
                    boleh:</li>
                <ul>
                    <li type="none">- memodifikasi atau menyalin materi;</li>
                    <li type="none">- menggunakan materi untuk tujuan komersial, atau untuk tampilan publik
                        (komersial atau non-komersial);</li>
                    <li type="none">- berupaya mendekompilasi atau merekayasa balik perangkat lunak apa pun yang
                        terdapat di situs web Simple School;</li>
                    <li type="none">- menghapus hak cipta atau notasi kepemilikan lainnya dari materi; atau
                        mentransfer materi ke orang lain atau "mirror" materi di server lain.</li>
                </ul>

                <li type="A">Lisensi ini akan berakhir secara otomatis jika Anda melanggar salah satu dari
                    pembatasan ini dan dapat dihentikan oleh Simple School kapan saja. Setelah mengakhiri
                    penayangan materi ini atau setelah penghentian lisensi ini, Anda harus memusnahkan semua
                    materi yang diunduh dalam kepemilikan Anda baik dalam format elektronik atau cetak.</li>
            </ol>
        </div>

        <div class="item-3 mb-4">
            <li>Penolakan</li>
            Segala hal yang ada di situs web Simple School disediakan atas dasar 'apa adanya'. Simple School
            tidak membuat jaminan, tersurat maupun tersirat, dan dengan ini menolak dan meniadakan semua jaminan
            lainnya termasuk, tanpa batasan, jaminan tersirat atau ketentuan yang dapat diperjualbelikan,
            kesesuaian untuk tujuan tertentu, atau non-pelanggaran kekayaan intelektual atau pelanggaran hak
            lainnya. Lebih jauh, Simple School tidak menjamin atau membuat pernyataan apa pun mengenai
            keakuratan, kemungkinan hasil, atau keandalan penggunaan materi di situs webnya atau yang terkait
            dengan materi tersebut atau di situs mana pun yang terhubung ke situs ini.
        </div>

        <div class="item-4 mb-4">
            <li>Batasan</li>
            Dalam keadaan apa pun, Simple School atau pemasoknya tidak bertanggung jawab atas segala kerusakan
            (termasuk, tanpa batasan, kerusakan karena kehilangan data atau laba, atau karena gangguan bisnis)
            yang timbul dari penggunaan atau ketidakmampuan untuk menggunakan materi di situs web Simple School,
            bahkan jika Simple School atau perwakilan Simple School yang resmi telah diberi tahu secara lisan
            atau tertulis tentang kemungkinan kerusakan tersebut. Karena beberapa yurisdiksi tidak mengizinkan
            pembatasan pada jaminan tersirat, atau batasan tanggung jawab atas kerusakan konsekuensial atau
            insidental, batasan ini mungkin tidak berlaku untuk Anda.
        </div>

        <div class="item-5 mb-4">
            <li>Akurasi</li>
            Materi yang muncul di situs web Simple School dapat mencakup kesalahan teknis, tipografi, atau
            fotografi. Simple School tidak menjamin bahwa salah satu materi di situs webnya akurat, lengkap atau
            terkini. Simple School dapat membuat perubahan pada materi yang terkandung di situs webnya kapan
            saja tanpa pemberitahuan. Namun Simple School tidak membuat komitmen untuk memperbarui materi.
        </div>

        <div class="item-6 mb-4">
            <li>Tautan</li>
            Simple School belum meninjau semua situs yang terhubung ke situs webnya dan tidak bertanggung jawab
            atas isi dari situs yang terhubung tersebut. Dimasukkannya tautan apa pun tidak menyiratkan dukungan
            oleh Simple School. Penggunaan situs web yang ditautkan tersebut merupakan risiko pengguna sendiri.
        </div>

        <div class="item-7 mb-4">
            <li>Modifikasi</li>
            Simple School dapat merevisi ketentuan layanan ini untuk situs webnya kapan saja tanpa
            pemberitahuan. Dengan menggunakan situs web ini, Anda setuju untuk terikat oleh versi terbaru dari
            ketentuan layanan ini.
        </div>

        <div class="item-8 mb-4">
            <li>Hukum yang mengatur</li>
            Syarat dan ketentuan ini diatur oleh dan ditafsirkan sesuai dengan hukum dan Anda tunduk pada
            yurisdiksi eksklusif pengadilan yang berlaku.
        </div>
    </ol>
</div>
@endsection
